﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using koiraohjelma;

namespace Koiratestit
{
    [TestFixture]
    public class Placetestit
    {
        private Place place;
        private DatabaseConnection tietokanta;

        [SetUp]
        public void setupTestit()
        {
            tietokanta = new DatabaseConnection("testi.sqlite");
            place = new Place(1, "Koirapuisto", 60.343, -13.242, "Kiva koirapuisto Riihimäellä", 40, 1, tietokanta);
        }

        [Test]
        public void checkPlaceName()
        {
            Assert.That("Koirapuisto", Is.EqualTo(place.name));
        }

        [Test]
        public void setPlaceName()
        {
            place.name = "Riksun koirapuisto";
            Assert.That("Riksun koirapuisto", Is.EqualTo(place.name));
        }

        [Test]
        public void checkPlaceCoords()
        {
            Assert.That(60.343, Is.EqualTo(place.latitude));
            Assert.That(-13.242, Is.EqualTo(place.longitude));
        }

        [Test]
        public void checkPlaceDescription()
        {
            Assert.That("Kiva koirapuisto Riihimäellä", Is.EqualTo(place.description));
        }

        [Test]
        public void setPlaceDescription()
        {
            place.description = "asdf";
            Assert.That("asdf", Is.EqualTo(place.description));
        }

        [Test]
        public void setSize()
        {
            place.size = 200;
            Assert.That(200, Is.EqualTo(place.size));
        }

        [Test]
        public void setWaterAvailability()
        {
            place.hasWater = 't';
            Assert.That('t', Is.EqualTo(place.hasWater));
        }

        [Test]
        public void setHousingNearby()
        {
            place.hasHousing = 't';
            Assert.That('t', Is.EqualTo(place.hasHousing));
        }

        [Test]
        public void setParking()
        {
            place.hasParking = 't';
            Assert.That('t', Is.EqualTo(place.hasParking));
        }

        [Test]
        public void setWinterUsable()
        {
            place.winterUsable = 't';
            Assert.That(true, Is.EqualTo(place.winterUsable));
        }

        [Test]
        public void setCategory()
        {
            place.category = 3;
            Assert.That(3, Is.EqualTo(place.category));
        }

        [Test]
        public void setDistanceToRoad()
        {
            place.distanceToRoad = 250;
            Assert.That(250, Is.EqualTo(place.distanceToRoad));
        }

        [Test]
        public void setTerrain()
        {
            place.setTerrain(6,2,5,1,7);
            int[] test = { 6, 2, 5, 1, 7 };
            Assert.That(test, Is.EqualTo(place.getTerrain()));
        }
    }
}
