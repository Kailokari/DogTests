﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using koiraohjelma;

namespace Koiratestit
{

    [TestFixture]
    public class DogTests
    {
        private Dog dog;
        private DatabaseConnection tietokanta;

        [SetUp]
        public void setupTestit()
        {
            tietokanta = new DatabaseConnection("testi.sqlite");
            dog = new Dog("PsPad", "leonberg", "Björn Tassen", "U", "2012", "rauhallinen", tietokanta); //PsPad virhe:cannot convert string to long? jatkossa Dog-ohjelmassa this.breed jne? userid, dogid, deleted?
        }

        /*breed = "default"; //dog.cs oletusmuodostimen tiedot ei this vielä
            kennel = "default";
            gender = "X";
            birthdate = 2000;
            personality = "default";
            deleted = "N";*/

        /*  [Test]
          public void checkUserid()
          {
              Assert.That("1", Is.EqualTo(dog.userId));
          }*/


        [Test]
        public void checkDogname()
        {
            Assert.That("PsPad", Is.EqualTo(dog.dogname));
        }

        [Test]
          public void checkKennelname()
          {
              Assert.That("Björn Tassen", Is.EqualTo(dog.kennel));
          }

        [Test]
        public void checkGender()
        {
            Assert.That("U", Is.EqualTo(dog.gender));
        }

        [Test]
        public void checkBirthdate()
        {
            Assert.That("2012", Is.EqualTo(dog.birthdate));
        }

        [Test]
        public void checkPersonality()
        {
            Assert.That("rauhallinen", Is.EqualTo(dog.personality));
        }


    }
}
