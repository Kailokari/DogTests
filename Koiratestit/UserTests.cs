﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using koiraohjelma;

namespace Koiratestit
{
    [TestFixture]
    public class UserTests
    {
        public User user;
        public Dog dog;
        private DatabaseConnection tietokanta;

        [SetUp]
        public void setUpTests()
        {
            tietokanta = new DatabaseConnection("testi.sqlite");
            user = new User("matti", "salainen", "Matti Meikäläinen", tietokanta);
            dog = new Dog("Musti", 1, tietokanta);

        }

        [Test]
        public void checkUsername()
        {
            Assert.That("matti", Is.EqualTo(user.username));
        }

        [Test]
        public void checkPassword()
        {
            Assert.That(true, Is.EqualTo(user.logIn("salainen")));
        }

        //Tietokantaa käyttävät testit eivät tällä hetkellä toimi jonkin dll-tiedoston puuttumisen takia.
        [Test]
        public void IsThisLastRow()
        {
            Assert.That(false, Is.EqualTo(dog.NotLastRow(3, tietokanta)));
        }
    }
}
