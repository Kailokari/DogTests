using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace koiraohjelma
{
    class Program
    {
    static User loggedinUser = null;
        static void Main(string[] args)
        {

            //Luodaan tietokantaolio
            DatabaseConnection tietokanta = new DatabaseConnection("hauhau.sqlite");
            //Tarkistetaan onko tietokanta olemassa
            try
            {
                //Koska tietokannan avaaminen luo sen aina, niin kokeillaan lukea sieltä jotakin
                //Jos heittää erroria, niin voidaan olettaa että tietokanta on tyhjä (=Luotiin juuri avattaessa)
                tietokanta.selectfromTableOrder_noReturn("*", "bookingdogs", "dogid", "desc");
            }
            //Jos ei ole tiedostoa, niin luodaan se ja tehdään siihen taulut
            catch
            {
                tietokanta.createTable("users", "personid integer primary key, username varchar(20), password varchar(20), name varchar(50)");
                //Jouduin muuttamaan birthdaten muuttujatyypin datetime -> int, että sain nykyisen version toimimaan. Jos jää aikaa tarpeeksi, voin koittaa saada toimimaan myöhemmin datetimen kanssa. -Tony
                tietokanta.createTable("dogs", "dogid integer primary key, userid int, dogname varchar(30), breed varchar(40), kennel varchar(40), gender char, birthdate int, personality varchar(255), deleted char");
                tietokanta.createTable("location", "locationid integer primary key, latitude float, longitude float, placename varchar(40), description varchar(255), siza int, hasWater char, hasHousing char, hasParking char, winterAble char,  category int, dinstanceToRoad int, terrain1 int, terrain2 int, terrain3 int, terrain4 int, terrain5 int");
                tietokanta.createTable("locationcategory", "categoryid integer primary key, categorydesc varchar(70)");
                tietokanta.createTable("place", "locationid integer primary key, latitude float, longitude float, placename varchar(40), description varchar(255), size int, hasWater char, hasHousing char, hasParking char, winterUsable char, category int, distanceToRoad int, terrain1 int, terrain2 int, terrain3 int, terrain4 int, terrain5 int");
                tietokanta.createTable("placecategory", "categoryid integer primary key, categorydesc varchar(70)");
                tietokanta.createTable("terrain", "terrainid integer primary key, terraindesc varchar(70)");
                tietokanta.createTable("locationComments", "commentid integer primary key, locationid int, userid int, comment varchar(255)");
                tietokanta.createTable("favouritePlaces", "userid int, locationid int");
                tietokanta.createTable("favouriteDogs", "userid int, dogid int");
                tietokanta.createTable("placebooking", "bookingid integer primary key, userid int, location int, latitude float, longitude float, bookingTime varchar(20)");
                tietokanta.createTable("bookingdogs", "bookingid int, dogid int");
                tietokanta.createTable("locationReviewMessages", "reviewid int, userid int, locationid int, comment varchar(255), reviewed char");

                tietokanta.insertToTable("placecategory", "1, \"Koirapuisto\"");
                tietokanta.insertToTable("placecategory", "2, \"Puisto\"");
                tietokanta.insertToTable("placecategory", "3, \"Metsikkö\"");
                tietokanta.insertToTable("placecategory", "4, \"Eläinlääkäri\"");
                tietokanta.insertToTable("placecategory", "5, \"Lemmikkieläinkauppa\"");

                tietokanta.insertToTable("terrain", "1, \"Tasainen maasto\"");
                tietokanta.insertToTable("terrain", "2, \"Mäkinen maasto\"");
                tietokanta.insertToTable("terrain", "3, \"Puro\"");
                tietokanta.insertToTable("terrain", "4, \"Lampi\"");
                tietokanta.insertToTable("terrain", "5, \"Puustoinen\"");
                tietokanta.insertToTable("terrain", "6, \"Ei esteitä\"");
                tietokanta.insertToTable("terrain", "7, \"Sopii pyörätuolille\"");
                tietokanta.insertToTable("terrain", "8, \"Aidattu alue\"");
                Console.WriteLine("Tietokanta ja taulut luotu.");

            }
            
            loggedinUser = sisaanKirjautuminen(tietokanta);

            while (true)
            {
                bool exit = paavalikko(tietokanta);
                if (exit)
                {
                    break;
                }
            }

            //
            //
            //  Osuuden "Koirien lisääminen ja hallinta" alku
            //
            //


            //Koiran lisääminen
            List<Dog> dogs = new List<Dog>();


            void NewDog(string dogname)
            {
                dogs.Add(new Dog(dogname, (int)loggedinUser.getId(), tietokanta));
                dogs[dogs.Count - 1].addToDataBaseDog();
            }

            //Tällä hetkellä dogid luetaan tietokannasta pelkästään userid:n mukaan, jolloin palautuu
            //käyttäjän ensimmäisen koiran id. Tarvitsee muokata myöhemmin, mutta oma osaaminen tällä
            //hetkellä rajoitettua tämän SQLiten syntaksin kanssa.

            NewDog("Musti");
            NewDog("Jake");
            NewDog("Hessu");
            NewDog("Sari");


            for (int i = 0; i < dogs.Count; i++)
            {
                Console.WriteLine(dogs[i]);
            }
            Console.WriteLine("Tietokantaan lisätty " + dogs.Count + " uutta koiraa.");
            Console.WriteLine("dogid on tällä hetkellä käyttäjän ensimmäisen koiran id. Korjataan myöhemmin.");
            Console.WriteLine("(tietokannasta haettaessa dogid:tä pelkän userid:n perusteella, tietokanta palauttaa ensimmäisen tuloksen)");
            Console.WriteLine();


            //Koirien tarkasteluun ja muokkaukseen käytettävä olio
            Dog currentdog = new Dog("default", (int)loggedinUser.getId(), tietokanta);



            //Koiramenu

            void DogMenu()
            {
                string selection = "0";
                while (selection != "6")
                {
                    Console.WriteLine();
                    Console.WriteLine("Mitä haluat tehdä?");
                    Console.WriteLine();
                    Console.WriteLine("1. Tarkastele kaikkia koiria (myöhemmin myös vain omia)");
                    Console.WriteLine("2. Lisää uusi koira");
                    Console.WriteLine("3. Poista koira");
                    Console.WriteLine("4. Muuta koiran tietoja");
                    Console.WriteLine("5. Hallinnoi koiran kuvia");
                    Console.WriteLine("6. Palaa päävalikkoon");
                    Console.WriteLine();
                    selection = Console.ReadLine();

                    if (selection == "1") currentdog.ViewAllDogs();
                    else if (selection == "2")
                    {
                        Console.WriteLine();
                        Console.WriteLine("Syötä koiran nimi:");
                        NewDog(Console.ReadLine());
                        Console.WriteLine();
                        Console.WriteLine("Koiran lisääminen onnistui.");
                        Console.WriteLine(dogs[dogs.Count - 1]);
                        Console.WriteLine("dogid korjataan myöhemmin. Näet kuitenkin oikean id:n tarkastelussa (paina 1)");
                    }
                    else if (selection == "3")
                    {
                        int currentdogid;
                        Console.WriteLine("Syötä poistettavan koiran id (placeholder):");
                        currentdogid = Int32.Parse(Console.ReadLine());
                        currentdog.deleted = "Y";
                        currentdog.ReplaceDogWithCurrentDog(currentdogid, tietokanta);
                        Console.WriteLine();
                        Console.WriteLine("Koira lisätty poistolistalle. Tämä on kumottavissa seuraavaan päivitykseen saakka.");
                    }

                    else if (selection == "4") EditDogUI();
                    else if (selection == "5") ; //TODO: kuvien hallinta
                    else if (selection == "6") ; //TODO: integraatio pääluuppiin.
                    else Console.WriteLine("Väärä valinta. Anna valinta 1 - 6");
                }

            }

            //Valitun koiran tietojen muokkaus

            void EditDogUI()
            {
                Console.WriteLine();
                Console.WriteLine("Valitse koira, jonka tietoja haluat muokata (syötä id):");
                int currentdogid = Int32.Parse(Console.ReadLine());
                currentdog.ReplaceCurrentDog(currentdogid, tietokanta);

                string selection = "0";
                while(selection != "7")
                {
                    Console.WriteLine();
                    Console.WriteLine("Mitä tietoja haluat muokata?");
                    Console.WriteLine("1. Koiran nimi");
                    Console.WriteLine("2. Koiran rotu");
                    Console.WriteLine("3. Kenneli");
                    Console.WriteLine("4. Koiran sukupuoli");
                    Console.WriteLine("5. Koiran syntymävuosi");
                    Console.WriteLine("6. Koiran persoonallisuus");
                    Console.WriteLine("7. Tallenna muutokset ja poistu muokkaustilasta");
                    Console.WriteLine();
                    selection = Console.ReadLine();

                    if (selection == "1")
                    {
                        Console.WriteLine("Syötä uusi nimi koiralle:");
                        currentdog.dogname = Console.ReadLine();
                    }

                    else if (selection == "2")
                    {
                        Console.WriteLine("Syötä koiran rotu:");
                        currentdog.breed = Console.ReadLine();
                    }

                    else if (selection == "3")
                    {
                        Console.WriteLine("Syötä koiran kenneli:");
                        currentdog.kennel = Console.ReadLine();
                    }

                    else if (selection == "4")
                    {
                        Console.WriteLine("Syötä koiran sukupuoli:");
                        currentdog.gender = Console.ReadLine();
                    }

                    else if (selection == "5")
                    {
                        Console.WriteLine("Syötä koiran syntymävuosi:");
                        currentdog.birthdate = Int32.Parse(Console.ReadLine());
                    }

                    else if (selection == "6")
                    {
                        Console.WriteLine("Syötä koiran persoonallisuus:");
                        currentdog.personality = Console.ReadLine();
                    }

                    else if (selection == "7")
                    {
                        currentdog.ReplaceDogWithCurrentDog(currentdogid, tietokanta);
                        Console.WriteLine("Muutokset tallennettu, poistutaan muokkaustilasta.");
                    }

                    else Console.WriteLine("Väärä valinta. Anna valinta 1 - 7");
                }
            }

            DogMenu();



            //TODO: Koirien poisto tietokannasta.

            //TODO: Testit

            //TODO: Koirien haku eri vaihtoehdoilla ja koiran valinta hakutuloksista.

            //TODO: Kuvien lisääminen (ja poisto)




            //
            //
            //  Osuuden "Koirien lisääminen ja hallinta" loppu
            //
            //

        }

        private static User sisaanKirjautuminen(DatabaseConnection tietokanta)
        {
            Console.WriteLine("Tervetuloa Koiraohjelmaan!");
            Console.WriteLine();
            Console.WriteLine("Käyttäjänimi:");
            //luetaan tässä käyttäjän käyttäjänimi
            string kayttajanimi = Console.ReadLine();
            string where = "username = \"" + kayttajanimi + "\"";
            //Haetaan käyttäjänimeä users-taulusta
            SQLiteDataReader usernametable = tietokanta.selectFromTableWhere("personid", "users", where);

            //Jos haku palauttaa rivejä, niin käyttäjä on olemassa
            if (usernametable.HasRows == true)
            {
                //Ladataan käyttäjän tiedot loggedinUser-olioon
                loggedinUser = new User(kayttajanimi, tietokanta);
                while (true)
                {
                    Console.WriteLine("Salasana: ");
                    string salasana = Console.ReadLine();
                    //Tarkistetaan onko salasana oikein
                    if (loggedinUser.logIn(salasana))
                    {
                        Console.WriteLine("Tervetuloa!");
                        return loggedinUser;
                    }
                    else
                    {
                        Console.WriteLine("Salasana väärin.");
                    }
                }
                //Jos rivejä ei ole olemassa, niin käyttäjä on uusi
            }
            else
            {
                Console.WriteLine("Olet uusi käyttäjä!");
                Console.WriteLine("Anna salasana:");
                string salasana = Console.ReadLine();
                Console.WriteLine("Anna nimesi");
                string nimi = Console.ReadLine();
                //Luodaan uusi käyttäjä loggedinUser-olioon
                loggedinUser = new User(kayttajanimi, salasana, nimi, tietokanta);
                //Ja lisätään hänet tietokantaan
                loggedinUser.addToDataBase();
                return loggedinUser;
            }
        }

        private static bool paavalikko(DatabaseConnection tietokanta)
        {
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Päävalikko");
                Console.WriteLine("Hei " + loggedinUser.name);
                Console.WriteLine("Olet sijainnissa " + loggedinUser.getCoordinates());
                Console.WriteLine();
                Console.WriteLine("1. Koirasi");
                Console.WriteLine("2. Päivitä nykyinen sijaintisi");
                Console.WriteLine("3. Lisää nykyinen sijaintisi paikaksi");
                Console.WriteLine("4. Lähellä olevat sijainnit");
                Console.WriteLine("5. Suosikkisijaintisi");
                Console.WriteLine("6. Suosikkikoirasi");
                Console.WriteLine("7. Kirjaudu ulos");
                Console.WriteLine("8. Lopeta");
                Console.WriteLine();
                Console.WriteLine("Valinta:");
                string valinta = Console.ReadLine();


                if (valinta == "2")
                {
                    kayttajanSijainninPaivitys();
                }
                else if (valinta == "3")
                {
                    lisaaPaikkaKayttajanSijainnista(tietokanta);
                }
                else if (valinta == "7")
                {
                    loggedinUser = sisaanKirjautuminen(tietokanta);
                }
                else if (valinta == "8")
                {
                    return true;
                }
                else
                {
                    Console.WriteLine("Virheellinen valinta");
                }
            }

            
        }

        public static void kayttajanSijainninPaivitys() {
            double latitude;
            double longitude;
            while (true) {
                try
                {
                    Console.WriteLine("Anna paikkasi latitude.");
                    Console.WriteLine("Muoto on 60.342123 pohjoiselle tai -60.34342 eteläiselle");
                    string syote = Console.ReadLine();
                    latitude = double.Parse(syote);
                    break;
                } catch
                {
                    Console.WriteLine("Virheellinen syote.");
                    Console.WriteLine();
                }
            }

            while (true)
            {
                try
                {
                    Console.WriteLine("Anna paikkasi longitude.");
                    Console.WriteLine("Muoto on 60.342123 läntiselle tai -60.34342 itäiselle");
                    string syote = Console.ReadLine();
                    longitude = double.Parse(syote);
                    break;
                }
                catch
                {
                    Console.WriteLine("Virheellinen syote.");
                    Console.WriteLine();
                }
            }

            loggedinUser.latitude = latitude;
            loggedinUser.longitude = longitude;
            
        }

        public static void lisaaPaikkaKayttajanSijainnista(DatabaseConnection tietokanta)
        {
            Console.WriteLine("Nykyinen sijaintisi on " + loggedinUser.getCoordinates());
            String syote;
            while (true)
            {
                Console.WriteLine("Lisätäänkö tämä sijainniksi? (K/E)");
                syote = Console.ReadLine();
                syote = syote.ToUpper();

                if (syote=="K")
                {
                    break;
                } else if (syote == "E")
                {
                    return;
                } else
                {
                    Console.WriteLine("Syote ei kelpaa");
                }
            }

            double latitude = loggedinUser.latitude;
            double longitude = loggedinUser.longitude;

            Console.WriteLine();
            Console.WriteLine("Anna paikan nimi:");
            string nimi = Console.ReadLine();
            Console.WriteLine("Anna paikan kuvaus:");
            string kuvaus = Console.ReadLine();

            int koko;
            while (true) {
                Console.WriteLine("Anna paikan arvioitu koko metreissä:");
                syote = Console.ReadLine();
                try
                {
                    koko = int.Parse(syote);
                    break;
                } catch
                {
                    Console.WriteLine("Virheellinen syöte");
                }
            }

            int kategoria;
            while (true)
            {
                Console.WriteLine("Mihin näistä kategorioista paikka kuuluu? ");
                PlaceCategories kategoriat = new PlaceCategories(tietokanta);
                kategoriat.printPlaceCategories();
                syote = Console.ReadLine();
                try
                {
                    kategoria = int.Parse(syote);
                    break;
                }
                catch
                {
                    Console.WriteLine("Virheellinen syöte");
                }
            }

            while (true)
            {
                Console.WriteLine("Haluatko antaa tarkempia tietoja? (K/E)");
                syote = Console.ReadLine();
                syote = syote.ToUpper();

                if (syote == "K")
                {
                    //Mennään ulos kyselyloopista ja jatketaan lisätietojen kyselyyn
                    break;
                }
                else if (syote == "E")
                {
                    //Tehdään tiedoista paikkaolio ja lisätään se tietokantaan
                    //Sen jälkeen poistutaan metodista.
                    Place paikka = new Place(nimi, latitude, longitude, kuvaus, koko, kategoria, tietokanta);
                    paikka.addToDataBase();
                    return;
                }
                else
                {
                    Console.WriteLine("Syote ei kelpaa");
                }
            }

            char vesi;
            char taloja;
            char parkkipaikka;
            char talvikaytettava;
            int matkaTielle;

            Console.WriteLine("Onko koiralle saatavilla vettä? (K/E)");
            syote = Console.ReadLine().ToUpper();
            if (syote == "K")
            {
                vesi = 't';
            }
            else
            {
                vesi = 'f';
            }

            Console.WriteLine("Onko paikassa lähellä taloja? (K/E)");
            syote = Console.ReadLine().ToUpper();
            if (syote == "K")
            {
                taloja = 't';
            }
            else
            {
                taloja = 'f';
            }

            Console.WriteLine("Onko paikassa parkkipaikkaa? (K/E)");
            syote = Console.ReadLine().ToUpper();
            if (syote == "K")
            {
                parkkipaikka = 't';
            }
            else
            {
                parkkipaikka = 'f';
            }

            Console.WriteLine("Onko paikka käytettävissä talvella? (K/E)");
            syote = Console.ReadLine().ToUpper();
            if (syote == "K")
            {
                talvikaytettava = 't';
            }
            else
            {
                talvikaytettava = 'f';
            }

            while (true) {
                Console.WriteLine("Kuinka pitkä matka paikasta on isommalle tielle metreissä:");
                syote = Console.ReadLine();
                try
                {
                    matkaTielle = int.Parse(syote);
                    break;
                }
                catch
                {
                    Console.WriteLine("Virheellinen syöte");
                }
            }

            int[] maasto = new int[5];
            Console.WriteLine("Mitkä näistä kuvaavat maastoa?");
            Console.WriteLine("Voit valita maksimissaan 5 kuvausta. Syöttämällä tyhjän numeron, lopetat kuvausten antamisen.");
            TerrainCategories maastokategoriat = new TerrainCategories(tietokanta);
            maastokategoriat.printTerrainCategories();

            bool lopeta = false;
            for (int i=0; i<5; i++)
            {
                while (true)
                {
                    Console.WriteLine("Anna halutun maastotyypin numero: ");
                    syote = Console.ReadLine();
                    if (syote == "")
                    {
                        lopeta = true;
                        break;
                    } else
                    {
                        try
                        {
                            int syoteInt = int.Parse(syote);
                            bool kaytettyJo = false;
                            //Tarkistetaan onko maastokategoria jo syötetty
                            for (int j =0; j<=i; j++)
                            {
                                
                                if (maasto[j]==syoteInt)
                                {
                                    Console.WriteLine("Syötit tämän kategorian jo kertaalleen.");
                                    kaytettyJo = true;
                                    break;
                                }
                            }

                            if(kaytettyJo==false)
                            {
                                maasto[i] = syoteInt;
                                break;
                            }

                        } catch
                        {
                            Console.WriteLine("Syöte ei ollut numero");
                        }
                    }
                }

                if (lopeta==true)
                {
                    break;
                }
            }
            Place kokopaikka = new Place(nimi, latitude, longitude, kuvaus, koko, kategoria, vesi, taloja, parkkipaikka, talvikaytettava, matkaTielle,  maasto, tietokanta);
            kokopaikka.addToDataBase();
        }
    }
}
