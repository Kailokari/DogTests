﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace koiraohjelma
{
    public class Place
    {
        private int id;
        public string name { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public string description { get; set; }
        public int size { get; set; }
        public char hasWater { get; set; }
        public char hasHousing { get; set; }
        public char hasParking { get; set; }
        public char winterUsable { get; set; }
        public int category { get; set; }
        public int distanceToRoad { get; set; }
        private int[] terrain;
        DatabaseConnection tietokanta;
        /*
        public User usersHere;
        public GoingTo UsersGomingHere;
        public <List>PlaceComments;
        */

        public Place(int id, string name, double latitude, double longitude, string description, int size, int category, DatabaseConnection tietokanta)
        {
            this.id = id;
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.description = description;
            this.size = size;
            this.category = category;
            this.tietokanta = tietokanta;

            //Nollaa muut tiedot
            this.hasWater = ' ';
            this.hasHousing= ' ';
            this.hasParking = ' ';
            this.winterUsable = ' ';
            this.distanceToRoad = -1;
            terrain = new int[5];
            terrain[0] = -1;
            terrain[1] = -1;
            terrain[2] = -1;
            terrain[3] = -1;
            terrain[4] = -1;
        }

        public Place(string name, double latitude, double longitude, string description, int size, int category, DatabaseConnection tietokanta)
        {
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.description = description;
            this.size = size;
            this.category = category;

            this.hasWater = ' ';
            this.hasHousing = ' ';
            this.hasParking = ' ';
            this.winterUsable = ' ';
            this.distanceToRoad = -1;
            this.tietokanta = tietokanta;
            terrain = new int[5];
            terrain[0] = -1;
            terrain[1] = -1;
            terrain[2] = -1;
            terrain[3] = -1;
            terrain[4] = -1;
        }

        public Place(string name, double latitude, double longitude, string description, int size, int category, char hasWater, char hasHousing, char hasParking, char winterUsable, int distanceToRoad, int[] terrain, DatabaseConnection tietokanta)
        {
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
            this.description = description;
            this.size = size;
            this.category = category;

            this.hasWater = hasWater;
            this.hasHousing = hasHousing;
            this.hasParking = hasParking;
            this.winterUsable = winterUsable;
            this.distanceToRoad = distanceToRoad;
            this.tietokanta = tietokanta;
            this.terrain = new int[5];
            this.terrain[0] = terrain[0];
            this.terrain[1] = terrain[1];
            this.terrain[2] = terrain[2];
            this.terrain[3] = terrain[3];
            this.terrain[4] = terrain[4];

        }

        public void addToDataBase()
        {
            //ToString(CultureInfo.InvariantCulture) parsettaa tekstin muotoon, jossa desimaalierottimena käytetään pistettä.
            //Tämä täytynee sitten muistaa infoa haettaessa mainita kaiketi?
            string valuestring = "\"" + name + "\" , " + latitude.ToString(CultureInfo.InvariantCulture) + ", " + longitude.ToString(CultureInfo.InvariantCulture) + ", \"" + description + "\" , " + size + ", \"" + hasWater + "\", \"" + hasHousing + "\", \"" + hasParking + "\", \"" + winterUsable + "\", " + category + ", " + distanceToRoad + ", " + terrain[0] + ", " + terrain[1] + ", " + terrain[2] + ", " + terrain[3] + ", " + terrain[4];
            tietokanta.insertToTableFields("place", "placename, latitude, longitude, description, size, hasWater, hasHousing, hasParking, winterUsable, category, distanceToRoad, terrain1, terrain2, terrain3, terrain4, terrain5", valuestring);
        }

        public void setTerrain(int terrain1, int terrain2, int terrain3, int terrain4, int terrain5)
        {
            terrain[0] = terrain1;
            terrain[1] = terrain2;
            terrain[2] = terrain3;
            terrain[3] = terrain4;
            terrain[4] = terrain5;
        }

        public int[] getTerrain()
        {
            return terrain;
        }
    }
}
