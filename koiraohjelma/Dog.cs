﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace koiraohjelma
{
    public class Dog 
    {
        //Tehdyt muutokset, jotta tietokannasta lukeminen toimii:
        //tietokannan luominen: birthdate (DateTime -> int)
        //gender (char -> string)
        //userid (long -> int)
        //deleted (char -> string)
        //Syytä en tiedä... Ainut keino jolla sain toimimaan korvaus-funktion.
        private long dogid;
        private int userid;
        public string dogname { get; set; }
        public string gender { get; set; }
        public string breed { get; set; }
        public string kennel { get; set; }
        //public DateTime birthdate { get; set; }
        public int birthdate { get; set; }
        public string personality { get; set; }
        public string deleted;


        public DatabaseConnection tietokanta;
        

        //Haku userid:n perusteella
        public Dog(long userid, DatabaseConnection tietokanta)
        {
            this.tietokanta = tietokanta;

            SQLiteDataReader koiralukija = tietokanta.selectFromTableWhere("*", "dogs", "userid = \"" + userid + "\"");

            koiralukija.Read();
            this.dogid = (long)koiralukija["dogid"];
            this.userid = (int)koiralukija["userid"];
            this.dogname = (string)koiralukija["dogname"];
            this.breed = (string)koiralukija["breed"];
            this.kennel = (string)koiralukija["kennel"];
            this.gender = (string)koiralukija["gender"];
            this.birthdate = (int)koiralukija["birthdate"];
            this.personality = (string)koiralukija["personality"];
            this.deleted = (string)koiralukija["deleted"];

        }


        //Oletusmuodostin (muut tiedot voi lisätä muokkaamalla myöhemmin)
        public Dog(string dogname, int userid, DatabaseConnection tietokanta, string breed, string kennel, string gender, int birthdate, string personality)
        {
            this.dogname = dogname;
            this.userid = userid;
            this.tietokanta = tietokanta;
            this.breed = breed;
            this.kennel = kennel;
            this.gender = gender;
            this.birthdate = birthdate;
            this.personality = personality;


           
            
            
            /*
             //oletusarvot
            breed = "default";
            kennel = "default";
            gender = "X";
            birthdate = 2000;
            personality = "default";
            deleted = "N";
            */


        }


        public override string ToString()
        {
            if (deleted != "Y")
                return "DogID: " + dogid + " || Name: " + dogname + " || Gender: " + gender + " || Breed: " + breed + " || Kennel: " + kennel + " || Birthdate: " + birthdate + " || Personality: " + personality;
            else return "DogID: " + dogid + " || Koira on tällä hetkellä poistotilassa. Tämä on vielä peruuttettavissa.";
        }



        //Nykyisen koiran tietojen korvaaminen toisen koiran tiedoilla dogid:n mukaan.
        //Voidaan silmukalla tulostaa sitten myöhemmin kaikkien tietokannassa olevien koirien tiedot.
        public void ReplaceCurrentDog(long dogid, DatabaseConnection tietokanta)
        {
            this.tietokanta = tietokanta;

            SQLiteDataReader koiralukija = tietokanta.selectFromTableWhere("*", "dogs", "dogid = \"" + dogid + "\"");
            if (koiralukija.HasRows)
            {
                koiralukija.Read();

                this.dogid = (long)koiralukija["dogid"];
                this.userid = (int)koiralukija["userid"];
                this.dogname = (string)koiralukija["dogname"];
                this.breed = (string)koiralukija["breed"];
                this.kennel = (string)koiralukija["kennel"];
                this.gender = (string)koiralukija["gender"];
                this.birthdate = (int)koiralukija["birthdate"];
                this.personality = (string)koiralukija["personality"];
                this.deleted = (string)koiralukija["deleted"];
            }
        }



        //Katsotaan onko luetulla rivillä dataa (vai onko tyhjä)
        public bool NotLastRow(long dogid, DatabaseConnection tietokanta)

        {
            this.tietokanta = tietokanta;
            SQLiteDataReader koiralukija = tietokanta.selectFromTableWhere("*", "dogs", "dogid = \"" + dogid + "\"");

            if (koiralukija.HasRows) return true;
            else return false;
        }

        public void ViewAllDogs()
        {
            bool jatkumisehto = true;
            int i = 0;
            while (jatkumisehto == true)
            {
                //tarkistetaan onko rivi1 (i=0), rivi2 (i=1), jne... tyhjä. NotLastRow palauttaa true, jos rivillä on jotain sisältöä.
                jatkumisehto = NotLastRow(i + 1, tietokanta);

                //Viimeinen objekti tulostuu tuplana ilman
                if (jatkumisehto == false) break;

                //Tulostetaan kaikki tietokannan koirat korvaamalla yhden koiran tietoja silmukassa.
                ReplaceCurrentDog(i + 1, tietokanta);
                Console.WriteLine(ToString());
                i++;
            }
        }

        public void ReplaceDogWithCurrentDog(long dogid, DatabaseConnection tietokanta)
        {
            //Selvennys DatabaseConnection-luokassa updateTableDog-metodin alla.
            string f1 = "dogname"; string f2 = "breed"; string f3 = "kennel"; string f4 = "gender"; string f5 = "birthdate"; string f6 = "personality"; string f7 = "deleted";
            string v1 = dogname; string v2 = breed; string v3 = kennel; string v4 = gender; string v5 = ""+birthdate; string v6 = personality; string v7 = deleted;
            string searchfield = "dogid";
            string searchvalue = "" + dogid;

            tietokanta.updateTableDog("dogs", f1, v1, f2, v2, f3, v3, f4, v4, f5, v5, f6, v6, f7, v7, searchfield, searchvalue);

        }


        public void CurrentDogStatus()
        {
            Console.WriteLine(ToString());
        }

        public int getuserId()
        {
            return userid;
        }


        public void addToDataBaseDog()
        {
            //muotoa "value1" , "value2", "value3"
            string valuestringD = $"\"{userid}\" , \"{dogname}\" , \"{breed}\" , \"{kennel}\" , \"{gender}\" , \"{birthdate}\" , \"{personality}\" , \"{deleted}\"";
            tietokanta.insertToTableFields("dogs", "userid, dogname, breed, kennel, gender, birthdate, personality, deleted", valuestringD);

            //Nyt kun on koira lisätty, niin voidaan hakea tuo tietokannan antama id
            //Tällä hetkellä palauttaa käyttäjän ensimmäisen koiran id:n. Voidaan korjata myöhemmin lisäämällä hakuehtoon myös koiran nimi.
            SQLiteDataReader koiralukija = tietokanta.selectFromTableWhere("*", "dogs", "userid = \"" + userid + "\"");
            koiralukija.Read();
            this.dogid = (long)koiralukija["dogid"];
        }
    }


}
