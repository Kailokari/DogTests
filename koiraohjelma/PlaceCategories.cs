﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace koiraohjelma
{
    class PlaceCategories
    {
        DatabaseConnection tietokanta;

        public PlaceCategories(DatabaseConnection tietokanta)
        {
            this.tietokanta = tietokanta;
        }

        public void printPlaceCategories()
        {
            SQLiteDataReader lukija = tietokanta.selectallfromTable("placecategory");

            while (lukija.Read())
            {
                Console.WriteLine(lukija["categoryid"] + ". " + lukija["categorydesc"]);
            }
        }
    }
}
