﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace koiraohjelma
{
    public class User
    {
        private long id;
        public string username { get; set; }
        private string password;
        public string name { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public DatabaseConnection tietokanta;
        public List<Dog> dogs;
        /*

        private List<Place> favouritePlaces;
        private List<Dog> favouriteDogs;
        private List<GoingTo> goingToList;*/

        public User(string username, string password, string name, DatabaseConnection tietokanta)
        {
            this.username = username;
            this.password = password;
            this.name = name;
            this.tietokanta = tietokanta;
            /*dogs = new List<Dog>();
            favouritePlaces = new List<Place>();
            favouriteDogs = new List<Dog>();
            goingToList = new List<GoingTo>();*/
        }

 
        
        //Lataa käyttäjä tietokannasta käyttäjätunnuksen perusteella
        public User(string username, DatabaseConnection tietokanta)
        {
            this.tietokanta = tietokanta;

            SQLiteDataReader lukija = tietokanta.selectFromTableWhere("*", "users", "username = \"" + username + "\"");
            
            lukija.Read();
            this.id = (long)lukija["personid"];
            this.username = (string)lukija["username"];
            this.password = (string)lukija["password"];
            this.name = (string)lukija["name"];
        }

        public bool logIn(string userInput)
        {
            return this.password == userInput;
        }

        public void addToDataBase()
        {
            string valuestring = "\"" + username + "\" , \"" + password + "\", \"" + name + "\"";
            tietokanta.insertToTableFields("users", "username, password, name", valuestring);

            //Nyt kun on käyttäjä lisätty, niin voidaan hakea tuo tietokannan antama id
            SQLiteDataReader lukija = tietokanta.selectFromTableWhere("*", "users", "username = \"" + username + "\"");
            lukija.Read();
            this.id = (long)lukija["personid"];
        }


        public long getId()
        {
            return this.id;
        }


        public string getCoordinates()
        {
            string coordinates;
            if (latitude > 0) {
                coordinates = latitude + "N";
            } else
            {
                coordinates = latitude + "S";
            }

            coordinates += " ";

            if (longitude>0)
            {
                coordinates += longitude + "W";
            } else
            {
                coordinates += longitude + "E";
            }

            return coordinates;
        }

        /*public void addDog (Dog dog)
        {
            dogs.Add(dog);
        }

        public void addFavouritePlace(Place place)
        {
            favouritePlaces.Add(place);
        }

        public void addFavouriteDogs(Dog dog)
        {
            favouriteDogs.Add(dog);
        }

        public void addGoingTo(GoingTo goingTo)
        {
            goingToList.Add(goingTo);
        }*/


    }
}
