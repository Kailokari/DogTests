﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace koiraohjelma
{
    class TerrainCategories
    {
        DatabaseConnection tietokanta;
        public TerrainCategories(DatabaseConnection tietokanta)
        {
            this.tietokanta = tietokanta;
        }

        public void printTerrainCategories()
        {
            SQLiteDataReader lukija = tietokanta.selectallfromTable("terrain");

            while (lukija.Read())
            {
                Console.WriteLine(lukija["terrainid"] + ". " + lukija["terraindesc"]);
            }
        }
    }
}
