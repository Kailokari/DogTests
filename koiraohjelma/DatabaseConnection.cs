﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace koiraohjelma
{
    public class DatabaseConnection
    {
        private SQLiteConnection m_dbConnection;
        private string databaseName;

        public DatabaseConnection(string database_name)
        {
            this.databaseName = database_name;
            m_dbConnection = new SQLiteConnection("Data Source=" + databaseName + ";Version=3;");
            m_dbConnection.Open();

        }

        public void createTable(string tableName, string fields)
        {
            //string sql = "create table " + tableName + "(" + fields + ")";
            //$-merkki mahdollistaa muuttujien sijoittamisen heittomerkkien sisällä aaltosuluilla
            string sql = $"create table {tableName}({fields})";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public void insertToTable(string table, string values)
        {
            //string sql = "insert into " + table + " values(" + values + ")";
            string sql = $"insert into {table} values( {values} )";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public void insertToTableFields(string table, string fields, string values)
        {
            //string sql = "insert into " + table + "( " + fields + ") values (" + values + ")"); 
            string sql = $"insert into {table} ( {fields} ) values( {values} )";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public SQLiteDataReader selectallfromTable(string tableName)
        {
            string sql = "select * from " + tableName;
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return reader;
        }

        public SQLiteDataReader selectfromTableOrder(string selection, string tableName, string field, string order)
        {
            //string sql = "select " + selection + " from " + tableName + " order by " + field + " " + order;
            string sql = $"select {selection} from {tableName} order by {field} {order}";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return reader;
        }

        public SQLiteDataReader selectFromTableWhere(string selection, string tableName, string where)
        {
            string sql = $"select {selection} from {tableName} where {where}";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return reader;
        }

        public void updateTable(string table, string updatedfield, string updatedvalue, string searchfield, string searchvalue)
        {
            //string sql = "update " + table + " SET " + updatedfield +"  = '" + updatedvalue + "' WHERE " + searchfield +" = " +searchvalue;
            string sql = $"UPDATE {table} SET {updatedfield} = '{updatedvalue}' WHERE {searchfield} = {searchvalue}";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public void updateTableDog(string table, string f1, string v1, string f2, string v2, string f3, string v3, string f4, string v4, string f5, string v5, string f6, string v6, string f7, string v7, string searchfield, string searchvalue)
        {
            //Syntaksi muotoa: UPDATE table SET field1 = 'value1', column_2 = 'value2' WHERE searchfield = searchvalue
            string sql = $"UPDATE {table} SET {f1} = '{v1}', {f2} = '{v2}', {f3} = '{v3}', {f4} = '{v4}', {f5} = '{v5}', {f6} = '{v6}', {f7} = '{v7}' WHERE {searchfield} = {searchvalue}";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public void selectfromTableOrder_noReturn(string selection, string tableName, string field, string order)
        {
            //string sql = "select " + selection + " from " + tableName + " order by " + field + " " + order;
            string sql = $"select {selection} from {tableName} order by {field} {order}";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public void closeDatabaseConnection()
        {
            m_dbConnection.Close();
        }


    }
}
